# Social Bank

The trial run of a bank of social media content for the Tor Project's social media channels. Initially, this content is text-based.

Created as a part of Hackweek 2022.

## Project Description
The goal of this project is to create a bank of varied social media content that can be easily sorted and/or searched in some meaningful way. The intention is to help the Communications team be able to easily find posts on certain topics or themes, and to potentially provide this content to the localization team such that we can post in a variety of languages.

## What are the skills required to work on this project?
- English as a written language
- Interest in creating social media content
- General knowledge about Tor, but you do not need to be highly technical
- Ability to provide and receive constrictive editing
- Creativity!

## Contributing
Contributions are open. Ideas for social media content for the Tor Project is welcome. Join us during the hackweek here in this pad: https://pad.riseup.net/p/hackweek-social-media-bank-83X6P-keep

## Content categories
We're currently working on the following categories of posts and welcome contributions to any of them:

- call to action (e.g., become a ux volunteer! become a localizer! become an alpha tester!)
- how to
- did you know
- memes
- fundraising
- announcements about regular events like relay operator meetups and annual "days" like data privacy day
- announcements about releases (maybe: creating a template for each kind of release)
- on this day in tor history
